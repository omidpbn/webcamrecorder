import React from "react";
import WebCamRecording from "../component/WebCamRecording/WebCamRecording";

function App() {
  return (
    <div className="container-fluid bg-dark">
      <WebCamRecording />
    </div>
  );
}

export default App;