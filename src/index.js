import React from "react";
import ReactDOM from "react-dom/client";
import App from "./container/App";

import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.js";
import 'react-toastify/dist/ReactToastify.css';
import "./assets/css/Style.css";

const root = ReactDOM.createRoot(document.getElementById("root"));

root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
