import React, { useRef, useState, useEffect } from "react";
import { useReactMediaRecorder } from "react-media-recorder";
import { ToastContainer, toast } from "react-toastify";
import axios from "axios";

const WebCamRecording = () => {
  const { startRecording, stopRecording, status, mediaBlobUrl, previewStream } =
    useReactMediaRecorder({
      video: true,
      facingMode: { exact: "environment" },
    });

  const [enable, setEnable] = useState(true);
  const [progress, setProgress] = useState(0);
  const [video, setVideo] = useState(0);

  const VideoPreview = ({ stream }) => {
    const videoRef = useRef(null);
    useEffect(() => {
      if (videoRef.current && stream) {
        videoRef.current.srcObject = stream;
      }
    }, [stream]);
    if (!stream) {
      return null;
    }

    return <video ref={videoRef} autoPlay />;
  };

  const Counter = () => {
    const [counter, setCounter] = useState(20);

    useEffect(() => {
      if (counter > 0) {
        setTimeout(() => setCounter(counter - 1), 1000);
      }
    }, [counter]);

    return <span className="text-danger">Recording {counter}s</span>;
  };

  const handleSubmit = (e) => {
    fetch(mediaBlobUrl)
      .then((response) => response.blob())
      .then((blob) => {
        setVideo(blob);
        e.preventDefault();
        const video = new FormData();
        video.append("video", blob);
        axios
          .post("http://exkenas.ir/api/upload", video, {
            onUploadProgress: (data) => {
              setProgress(Math.round((100 * data.loaded) / data.total));
            },
          })
          .then((response) => {
            // console.log("Uploaded", response);
            toast.success("آپلود ویدیو با موفقیت انجام شد", {
              position: "top-right",
              theme: "colored",
              className: "toast-status",
            });
          })
          .catch((error) => {
            // console.log("No", error);
            toast.success("آپلود ویدیو با خطارو به رو شد!!! مجددا تلاش کنید", {
              position: "top-right",
              theme: "colored",
              className: "toast-status",
            });
          });
      });
  };

  return (
    <div className="col-lg-12 col-12 py-5">
      <h1 className="text-center text-white py-2">Webcam Recording</h1>
      <div className="col-lg-6 col-12 mx-auto">
        <div className="col-lg-12 col-12">
          <p
            className={
              status === "recording"
                ? "text-danger fw-bold"
                : "text-success fw-bold"
            }
          >
            {status === "idle" ? (
              ""
            ) : status === "recording" ? (
              <Counter />
            ) : status === "acquiring_media" ? (
              "Start Recording"
            ) : status === "stopped" ? (
              "End Record"
            ) : (
              ""
            )}
          </p>
        </div>

        <div className="col-lg-12 col-12">
          {status === "recording"
            ? enable && <VideoPreview stream={previewStream} />
            : ""}

          {status === "stopped" ? (
            <>
              <video src={mediaBlobUrl} controls autoPlay loop />

              <div className="progress mb-3">
                <div
                  className="progress-bar progress-bar-striped"
                  role="progressbar"
                  style={{ width: `${progress}%` }}
                  aria-valuenow={progress}
                  aria-valuemin="0"
                  aria-valuemax="100"
                >
                  {`${progress}%`}
                </div>
              </div>

              <p className="text-white fw-bold text-center">
                Video Size: {video.size} KB
              </p>

              <button
                className="btn btn-success fw-bold"
                onClick={handleSubmit}
              >
                Upload
              </button>

              <ToastContainer />
            </>
          ) : (
            ""
          )}
        </div>

        <div className="text-center">
          {status === "recording" ? (
            <button className="btn-stop" onClick={stopRecording} />
          ) : (
            <button
              className="btn-record"
              onClick={() => {
                startRecording();
                setTimeout(stopRecording, 22000);
                setEnable(true);
              }}
            />
          )}
        </div>
      </div>
    </div>
  );
};

export default WebCamRecording;
